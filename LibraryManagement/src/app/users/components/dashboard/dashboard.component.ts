import { AuthService } from './../../../auth/auth.service';
import { BooksModule } from './../../../books/books.module';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { TitleCasePipe } from '@angular/common';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
 admin=true;
 title=localStorage.getItem('userEmail')

 tit=this.title.toUpperCase();
  constructor( private router:Router, private auth:AuthService,) { }
  
  ngOnInit() {
    // this.admin = this.auth.isLoggedIn();
    this.admin = this.auth.getCurrentUser();
    this.auth.adminchangeEvent.subscribe(admin =>{
    this.admin = admin;
    console.log("hii")
    
    });
  }
  activeView="user";
  setView (viewName){
    this.activeView = viewName
  }
  onLogout (){
    // this.auth.logout();
    sessionStorage.clear();
    localStorage.clear();

    this.router.navigate(['/login']);
  }
  // navigateUser(){
  //   this.router.navigate(['/addusers']);
  // }
  // navigateBook(){
  //   this.router.navigate(['/addbooks']);
  // }
}
