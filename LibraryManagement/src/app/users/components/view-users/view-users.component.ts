import { AddserviceService } from './../../../addservice.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {

  constructor(private service:AddserviceService) { }
 users
  ngOnInit() {
    this.getdata()
  }

  getdata() {
    this.service.getCoins().subscribe(res => {
    this.users = res;
    });
    }

}
